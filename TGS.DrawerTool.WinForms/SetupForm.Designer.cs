﻿namespace TGS.DrawerTool.WinForms
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.browseDrawersBaseButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.drawersBaseLabel = new System.Windows.Forms.Label();
            this.drawersBaseTextBox = new System.Windows.Forms.TextBox();
            this.allowUpdatesCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // browseDrawersBaseButton
            // 
            resources.ApplyResources(this.browseDrawersBaseButton, "browseDrawersBaseButton");
            this.browseDrawersBaseButton.Name = "browseDrawersBaseButton";
            this.browseDrawersBaseButton.UseVisualStyleBackColor = true;
            this.browseDrawersBaseButton.Click += new System.EventHandler(this.browseDrawersBaseButton_Click);
            // 
            // acceptButton
            // 
            resources.ApplyResources(this.acceptButton, "acceptButton");
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // cancelButton
            // 
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // drawersBaseLabel
            // 
            resources.ApplyResources(this.drawersBaseLabel, "drawersBaseLabel");
            this.drawersBaseLabel.Name = "drawersBaseLabel";
            // 
            // drawersBaseTextBox
            // 
            resources.ApplyResources(this.drawersBaseTextBox, "drawersBaseTextBox");
            this.drawersBaseTextBox.Name = "drawersBaseTextBox";
            // 
            // allowUpdatesCheckBox
            // 
            resources.ApplyResources(this.allowUpdatesCheckBox, "allowUpdatesCheckBox");
            this.allowUpdatesCheckBox.Name = "allowUpdatesCheckBox";
            this.allowUpdatesCheckBox.UseVisualStyleBackColor = true;
            // 
            // SetupForm
            // 
            this.AcceptButton = this.acceptButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.Controls.Add(this.allowUpdatesCheckBox);
            this.Controls.Add(this.drawersBaseTextBox);
            this.Controls.Add(this.drawersBaseLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.browseDrawersBaseButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button browseDrawersBaseButton;
        private Button acceptButton;
        private Button cancelButton;
        private Label drawersBaseLabel;
        private TextBox drawersBaseTextBox;
        private CheckBox allowUpdatesCheckBox;
    }
}