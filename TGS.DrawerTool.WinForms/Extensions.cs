﻿using System.Collections.Specialized;
using System.Diagnostics;

namespace TGS.DrawerTool.WinForms
{
    internal static class Extensions
    {
        internal static void Remove(this List<string> collection, string value, IEqualityComparer<string> comparer)
        {
            var i = 0;
            foreach (var item in collection)
            {
                if (comparer.Equals(item, value))
                {
                    collection.RemoveAt(i);
                    return;
                }
                i++;
            }
        }

        /// <summary>
        /// Open this URI in the default browser.
        /// </summary>
        /// <param name="uri">
        /// The URI to open.
        /// </param>
        internal static void OpenInBrowser(this Uri uri)
        {
            Process.Start(new ProcessStartInfo("cmd.exe", $"/C start {uri.ToString().Replace("&", "^&")}")
            {
                CreateNoWindow = true
            });
        }

    }
}
