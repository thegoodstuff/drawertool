﻿using TGS.DrawerTool.WinForms.Resources.Strings;

namespace TGS.DrawerTool.WinForms
{
    public partial class DrawerCreateForm : Form
    {
        public string DrawersPath { get; }

        public string DrawerName { get => nameTextBox.Text; set => nameTextBox.Text = value; }

        public DrawerCreateForm(string drawersPath)
        {
            InitializeComponent();
            DrawersPath = drawersPath;
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            var path = Path.Join(DrawersPath, DrawerName);
            if (Directory.Exists(path))
            {
                MessageBox.Show(this, Strings.Error_DrawerNameTaken, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (IOException ex)
            {
                MessageBox.Show(this, ex.Message, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
