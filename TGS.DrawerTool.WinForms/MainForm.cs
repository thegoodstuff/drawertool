using System.Diagnostics;
using System.Reflection;
using System.Text.Json;
using System.Web;
using TGS.DrawerTool.WinForms.Resources.AppResources;
using TGS.DrawerTool.WinForms.Resources.Strings;

namespace TGS.DrawerTool.WinForms
{
    public partial class MainForm : Form
    {
        private string drawersPath = "";
        private HttpClient httpClient = new HttpClient();

        public MainForm()
        {
            InitializeComponent();
            Text = AppResources.AppName;
            appToolStripMenuItem.Text = AppResources.AppName;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var drawersPath = Settings.Content.DrawersPath;
            if (string.IsNullOrEmpty(drawersPath))
            {
                // Need to select the drawers folder
                var setupForm = new SetupForm();
                setupForm.AllowUpdates = true;
                if (setupForm.ShowDialog() != DialogResult.OK)
                {
                    this.drawersPath = "";
                    Close();
                    Application.Exit();
                    return;
                }
                this.drawersPath = Environment.ExpandEnvironmentVariables(setupForm.DrawersBasePath);
                Settings.Content.DrawersPath = this.drawersPath;
                Settings.Content.AllowUpdates = setupForm.AllowUpdates;
                Settings.Save();
            }
            else
            {
                this.drawersPath = drawersPath;
            }

            autoUpdateToolStripMenuItem.Checked = Settings.Content.AllowUpdates;
            if (Settings.Content.AllowUpdates) CheckUpdates(false);

            if (!Directory.Exists(this.drawersPath))
            {
                Directory.CreateDirectory(this.drawersPath);
            }

            // write readme if drawers folder is empty
            if (!Directory.EnumerateFileSystemEntries(this.drawersPath).Any())
            {
                File.WriteAllText(Path.Combine(this.drawersPath, Strings.DrawerFolderReadme_Name), Strings.DrawerFolderReadme);
            }

            httpClient.DefaultRequestHeaders.Add("User-Agent", $"{AppResources.AppName}/{FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion}");

            UpdateDrawers();
            UpdatePlaces();
        }

        private void UpdateDrawers()
        {
            drawersListBox.Items.Clear();
            foreach (var dir in Directory.GetDirectories(drawersPath))
                drawersListBox.Items.Add(new DirectoryInfo(dir).Name);
        }

        private void UpdatePlaces()
        {
            placesListBox.Items.Clear();
            if (Settings.Content.Places == null)
            {
                Settings.Content.Places = new List<string>();
                Settings.Save();
            }
            foreach (var place in Settings.Content.Places)
                if (place != null) placesListBox.Items.Add(place);
        }
        
        private async void CheckUpdates(bool alertIfNotFound = false)
        {
            try
            {
                // API docs: https://docs.gitlab.com/ee/api/releases/#list-releases
                var releasesString = await httpClient.GetStringAsync($"https://gitlab.com/api/v4/projects/{HttpUtility.UrlEncode(AppResources.GitLabProjectId)}/releases");

                Version? currentVersion = Assembly.GetEntryAssembly()?.GetName().Version;
                Version? largestVersion = null;
                JsonElement? latestRelease = null;
                
                var releases = JsonSerializer.Deserialize<JsonElement[]>(releasesString);
                if (releases != null)
                {
                    foreach (var release in releases)
                    {
                        string? versionString = null;
                        if (release.TryGetProperty("name", out var versionName) && (versionString = versionName.GetString()) != null)
                        {
                            var version = new Version(versionString);
                            if (largestVersion == null || version > largestVersion)
                            {
                                largestVersion = version;
                                latestRelease = release;
                            }
                        }
                    }

                    if (largestVersion != null && latestRelease != null && (currentVersion == null || largestVersion > currentVersion))
                    {
                        var updateUriString = ((JsonElement)latestRelease).GetProperty("_links").GetProperty("self").GetString();
                        if (updateUriString == null) throw new UriFormatException("Update page URL not found");
                        var updateUri = new Uri(updateUriString);
                        
                        if (MessageBox.Show(string.Format(Strings.Update_Available, largestVersion.ToString()), string.Format(Strings.Update_Available_Title, largestVersion.ToString()), MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            updateUri.OpenInBrowser();
                        }
                    }
                    else if (alertIfNotFound)
                    {
                        MessageBox.Show(Strings.Update_NoneAvailable, Strings.Update_NoneAvailable_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is JsonException || ex is HttpRequestException || ex is KeyNotFoundException || ex is UriFormatException)
                {
                    MessageBox.Show(Strings.Error_UpdateCheckFailed, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    throw;
                }
            }
        }

        private string? GetDrawerNameInPlace(string place)
        {
            var dirinfo = new DirectoryInfo(place);
            return dirinfo.LinkTarget == null ? null : Path.GetFileName(dirinfo.LinkTarget);
        }

        private void DoDrawerAssign(string? path = null)
        {
            var defaultDrawerName = (string)drawersListBox.Items[0];
            if (drawersListBox.SelectedItem != null) defaultDrawerName = (string)drawersListBox.SelectedItem;

            var form = new LinkForm(drawersPath);
            foreach (var drawerName in drawersListBox.Items) form.Drawers.Add(drawerName);
            form.SelectedDrawer = defaultDrawerName;

            if (path != null)
            {
                form.PlacePath = path;
                form.AllowPathChange = false;
            }

            form.ShowDialog();
            UpdatePlaces();
        }

        private void DoDeleteAssign(string path)
        {
            Directory.Delete(path);
            Directory.CreateDirectory(path);
            Settings.Content.Places?.Remove(path);
        }

        private void createDrawerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new DrawerCreateForm(drawersPath).ShowDialog();
            UpdateDrawers();
        }

        private void deleteDrawerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawersListBox.SelectedItem == null)
            {
                MessageBox.Show(this, Strings.Error_SelectDrawer, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var drawerName = (string)drawersListBox.SelectedItem;
            
            if (MessageBox.Show(this, string.Format(Strings.Confirm_DeleteDrawer, drawerName), string.Format(Strings.Confirm_DeleteDrawer_Title, drawerName), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Directory.Delete(Path.Combine(drawersPath, drawerName), true);
                foreach (var place in placesListBox.Items)
                {
                    var drawerNameInPlace = GetDrawerNameInPlace((string)place);
                    if (drawerNameInPlace == drawerName)
                    {
                        DoDeleteAssign((string)place);
                    }
                }
                Settings.Save();
                UpdateDrawers();
                UpdatePlaces();
            }
        }

        private void openDrawerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawersListBox.SelectedItem == null)
            {
                MessageBox.Show(this, Strings.Error_SelectDrawer, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Process.Start("explorer.exe", $"\"{Path.Combine(drawersPath, (string)drawersListBox.SelectedItem)}\"");
        }

        private void assignDrawerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (drawersListBox.Items.Count == 0)
            {
                MessageBox.Show(this, Strings.Error_NoDrawers, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DoDrawerAssign();
        }

        private void createPlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Same as Drawer -> Assign
            assignDrawerToolStripMenuItem_Click(sender, e);
        }

        private void deletePlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (placesListBox.SelectedItem == null)
            {
                MessageBox.Show(this, Strings.Error_SelectDrawer, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var path = (string)placesListBox.SelectedItem;
            var linkedDrawerName = GetDrawerNameInPlace(path);
            if (linkedDrawerName == null) linkedDrawerName = "";

            if (MessageBox.Show(this, string.Format(Strings.Confirm_DeletePlace, path, linkedDrawerName), string.Format(Strings.Confirm_DeletePlace_Title, path, linkedDrawerName), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DoDeleteAssign(path);
                Settings.Save();
                UpdatePlaces();
            }
        }

        private void relinkPlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (placesListBox.SelectedItem == null)
            {
                MessageBox.Show(this, Strings.Error_SelectDrawer, Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DoDrawerAssign((string)placesListBox.SelectedItem);
        }

        private void drawersListBox_DoubleClick(object sender, EventArgs e)
        {
            // On double-click, open the currently selected drawer in Explorer
            if (drawersListBox.SelectedItem != null)
                openDrawerToolStripMenuItem_Click(sender, e);
        }

        private void drawersListBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Keyboard shortcuts (only when a drawer is selected)
            if (drawersListBox.SelectedItem != null)
            {
                switch (e.KeyCode)
                {
                    // Enter - Open drawer in Explorer
                    case Keys.Enter:
                        openDrawerToolStripMenuItem_Click(sender, e);
                        break;

                    // Backspace, Delete - Delete drawer
                    case Keys.Back:
                    case Keys.Delete:
                        deleteDrawerToolStripMenuItem_Click(sender, e);
                        break;
                }
            }
        }

        private void placesListBox_DoubleClick(object sender, EventArgs e)
        {
            // On double-click, relink the currently selected place
            if (placesListBox.SelectedItem != null)
                relinkPlaceToolStripMenuItem_Click(sender, e);
        }

        private void placesListBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Keyboard shortcuts (only when a place is selected)
            if (placesListBox.SelectedItem != null)
            {
                switch (e.KeyCode)
                {
                    // Enter - Relink place
                    case Keys.Enter:
                        relinkPlaceToolStripMenuItem_Click(sender, e);
                        break;

                    // Backspace, Delete - Delete place
                    case Keys.Back:
                    case Keys.Delete:
                        deletePlaceToolStripMenuItem_Click(sender, e);
                        break;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void manualUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckUpdates(true);
        }

        private void autoUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Content.AllowUpdates = !Settings.Content.AllowUpdates;
            Settings.Save();
            autoUpdateToolStripMenuItem.Checked = Settings.Content.AllowUpdates;
        }
    }
}