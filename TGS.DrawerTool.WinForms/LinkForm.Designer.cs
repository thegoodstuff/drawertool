﻿namespace TGS.DrawerTool.WinForms
{
    partial class LinkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkForm));
            this.placePathTextBox = new System.Windows.Forms.TextBox();
            this.placePathLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.browsePlacePathButton = new System.Windows.Forms.Button();
            this.drawerLabel = new System.Windows.Forms.Label();
            this.drawerComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // placePathTextBox
            // 
            resources.ApplyResources(this.placePathTextBox, "placePathTextBox");
            this.placePathTextBox.Name = "placePathTextBox";
            // 
            // placePathLabel
            // 
            resources.ApplyResources(this.placePathLabel, "placePathLabel");
            this.placePathLabel.Name = "placePathLabel";
            // 
            // cancelButton
            // 
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // acceptButton
            // 
            resources.ApplyResources(this.acceptButton, "acceptButton");
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // browsePlacePathButton
            // 
            resources.ApplyResources(this.browsePlacePathButton, "browsePlacePathButton");
            this.browsePlacePathButton.Name = "browsePlacePathButton";
            this.browsePlacePathButton.UseVisualStyleBackColor = true;
            this.browsePlacePathButton.Click += new System.EventHandler(this.browsePlacePathButton_Click);
            // 
            // drawerLabel
            // 
            resources.ApplyResources(this.drawerLabel, "drawerLabel");
            this.drawerLabel.Name = "drawerLabel";
            // 
            // drawerComboBox
            // 
            resources.ApplyResources(this.drawerComboBox, "drawerComboBox");
            this.drawerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drawerComboBox.FormattingEnabled = true;
            this.drawerComboBox.Name = "drawerComboBox";
            // 
            // LinkForm
            // 
            this.AcceptButton = this.acceptButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.Controls.Add(this.drawerComboBox);
            this.Controls.Add(this.drawerLabel);
            this.Controls.Add(this.placePathTextBox);
            this.Controls.Add(this.placePathLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.browsePlacePathButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LinkForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox placePathTextBox;
        private Label placePathLabel;
        private Button cancelButton;
        private Button acceptButton;
        private Button browsePlacePathButton;
        private Label drawerLabel;
        private ComboBox drawerComboBox;
    }
}