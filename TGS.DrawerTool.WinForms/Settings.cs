﻿using System.Text.Json;

namespace TGS.DrawerTool.WinForms
{
    internal static class Settings
    {
        private static readonly JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        private static readonly string savePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TGS", "DrawerTool", "settings.json");

        internal class SettingsContent
        {
            public string? DrawersPath { get; set; } = null;

            public bool AllowUpdates { get; set; } = false;
            
            public List<string>? Places { get; set; } = null;
        }

        internal static SettingsContent Content { get; set; } = new SettingsContent();

        internal static void Read()
        {
            if (Directory.Exists(Path.GetDirectoryName(savePath)) && File.Exists(savePath))
            {
                using (var file = File.Open(savePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    try
                    {
                        var content = JsonSerializer.Deserialize<SettingsContent>(file, jsonSerializerOptions);
                        if (content == null) Content = new SettingsContent();
                        else Content = content;
                    }
                    catch (JsonException)
                    {
                        Content = new SettingsContent();
                    }
                }
            }
            else
            {
                Content = new SettingsContent();
            }
        }

        internal static void Save()
        {
            if (!Directory.Exists(Path.GetDirectoryName(savePath))) Directory.CreateDirectory(Path.GetDirectoryName(savePath));

            using (var file = File.Open(savePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                JsonSerializer.Serialize(file, Content, jsonSerializerOptions);
        }
    }
}
