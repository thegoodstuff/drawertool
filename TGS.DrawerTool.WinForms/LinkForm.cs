﻿using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;
using TGS.DrawerTool.WinForms.Resources.Strings;

namespace TGS.DrawerTool.WinForms
{
    public partial class LinkForm : Form
    {
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        private const int BCM_SETSHIELD = 0x160C;

        public string DrawersBasePath { get; set; }

        public string PlacePath { get => placePathTextBox.Text; set => placePathTextBox.Text = value; }

        public bool AllowPathChange
        {
            get => placePathTextBox.Enabled;
            set
            {
                placePathTextBox.Enabled = value;
                browsePlacePathButton.Enabled = value;
            }
        }

        public object? SelectedDrawer { get => drawerComboBox.SelectedItem; set => drawerComboBox.SelectedItem = value; }

        public ComboBox.ObjectCollection Drawers { get => drawerComboBox.Items; }

        public LinkForm(string drawersBasePath)
        {
            InitializeComponent();
            
            DrawersBasePath = drawersBasePath;

            // Detect if app is running as admin
            var windowsId = WindowsIdentity.GetCurrent();
            var isAdmin = windowsId == null ? true : new WindowsPrincipal(windowsId).IsInRole(WindowsBuiltInRole.Administrator);
            if (!isAdmin)
            {
                // Add UAC icon next to accept button if not admin
                acceptButton.FlatStyle = FlatStyle.System;
                SendMessage(acceptButton.Handle, BCM_SETSHIELD, 0, 1);
            }

            // Default path: %appdata%\.minecraft\mods
            PlacePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft", "mods");
        }

        private void browsePlacePathButton_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.SelectedPath = PlacePath;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                PlacePath = dialog.SelectedPath;
            }
        }

        private async void acceptButton_Click(object sender, EventArgs e)
        {
            if (SelectedDrawer == null) return;
            
            var dirExists = false;
            var isSymlinked = false;
            if (
                // If the directory exists, is not a symlink, and contains files then the first condition will not be met
                // which will cause the warning messagebox to show up.
                !(
                    (dirExists = Directory.Exists(PlacePath)) && // Directory exists (result is stored for later use)
                    (isSymlinked = new DirectoryInfo(PlacePath).LinkTarget == null) && // Directory is not a symlink (result is stored for later use)
                    Directory.EnumerateFileSystemEntries(PlacePath).Any() // Directory is not empty
                )
                ||
                MessageBox.Show(this, string.Format(Strings.Warning_PlaceHasFiles, PlacePath), string.Format(Strings.Warning_PlaceHasFiles_Title, PlacePath), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes
            )
            {
                if (dirExists)
                {
                    // If the directory is symlinked, any contents should not be deleted
                    Directory.Delete(PlacePath, isSymlinked);
                }

                var success = true;

                var createLinkStartInfo = new ProcessStartInfo("cmd");
                createLinkStartInfo.ArgumentList.Add("/C");
                createLinkStartInfo.ArgumentList.Add("mklink");
                createLinkStartInfo.ArgumentList.Add("/D");
                createLinkStartInfo.ArgumentList.Add(PlacePath);
                createLinkStartInfo.ArgumentList.Add(Path.Combine(DrawersBasePath, (string)SelectedDrawer));
                createLinkStartInfo.UseShellExecute = true;
                createLinkStartInfo.Verb = "runas";
                try
                {
                    var process = Process.Start(createLinkStartInfo);
                    if (process == null) success = false;
                    else await process.WaitForExitAsync();
                }
                catch (Win32Exception)
                {
                    success = false;
                }
                
                if (success)
                {
                    if (Settings.Content.Places != null && !Settings.Content.Places.Contains(PlacePath, StringComparer.OrdinalIgnoreCase))
                    {
                        Settings.Content.Places.Add(PlacePath);
                        Settings.Save();
                    }
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    if (Settings.Content.Places != null && Settings.Content.Places.Contains(PlacePath, StringComparer.OrdinalIgnoreCase))
                    {
                        Settings.Content.Places.Remove(PlacePath, StringComparer.OrdinalIgnoreCase);
                        Settings.Save();
                    }
                    Directory.CreateDirectory(PlacePath);
                    DialogResult = DialogResult.Abort;
                }

                Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
