﻿namespace TGS.DrawerTool.WinForms
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.appToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createDrawerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDrawerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDrawerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignDrawerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.placesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createPlaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePlaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relinkPlaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.drawersGroupBox = new System.Windows.Forms.GroupBox();
            this.drawersListBox = new System.Windows.Forms.ListBox();
            this.placesGroupBox = new System.Windows.Forms.GroupBox();
            this.placesListBox = new System.Windows.Forms.ListBox();
            this.mainMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.drawersGroupBox.SuspendLayout();
            this.placesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.appToolStripMenuItem,
            this.drawersToolStripMenuItem,
            this.placesToolStripMenuItem});
            resources.ApplyResources(this.mainMenuStrip, "mainMenuStrip");
            this.mainMenuStrip.Name = "mainMenuStrip";
            // 
            // appToolStripMenuItem
            // 
            this.appToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updatesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.appToolStripMenuItem.Name = "appToolStripMenuItem";
            resources.ApplyResources(this.appToolStripMenuItem, "appToolStripMenuItem");
            // 
            // updatesToolStripMenuItem
            // 
            this.updatesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualUpdateToolStripMenuItem,
            this.autoUpdateToolStripMenuItem});
            this.updatesToolStripMenuItem.Name = "updatesToolStripMenuItem";
            resources.ApplyResources(this.updatesToolStripMenuItem, "updatesToolStripMenuItem");
            // 
            // manualUpdateToolStripMenuItem
            // 
            this.manualUpdateToolStripMenuItem.Name = "manualUpdateToolStripMenuItem";
            resources.ApplyResources(this.manualUpdateToolStripMenuItem, "manualUpdateToolStripMenuItem");
            this.manualUpdateToolStripMenuItem.Click += new System.EventHandler(this.manualUpdateToolStripMenuItem_Click);
            // 
            // autoUpdateToolStripMenuItem
            // 
            this.autoUpdateToolStripMenuItem.Name = "autoUpdateToolStripMenuItem";
            resources.ApplyResources(this.autoUpdateToolStripMenuItem, "autoUpdateToolStripMenuItem");
            this.autoUpdateToolStripMenuItem.Click += new System.EventHandler(this.autoUpdateToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // drawersToolStripMenuItem
            // 
            this.drawersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDrawerToolStripMenuItem,
            this.deleteDrawerToolStripMenuItem,
            this.openDrawerToolStripMenuItem,
            this.assignDrawerToolStripMenuItem});
            this.drawersToolStripMenuItem.Name = "drawersToolStripMenuItem";
            resources.ApplyResources(this.drawersToolStripMenuItem, "drawersToolStripMenuItem");
            // 
            // createDrawerToolStripMenuItem
            // 
            this.createDrawerToolStripMenuItem.Name = "createDrawerToolStripMenuItem";
            resources.ApplyResources(this.createDrawerToolStripMenuItem, "createDrawerToolStripMenuItem");
            this.createDrawerToolStripMenuItem.Click += new System.EventHandler(this.createDrawerToolStripMenuItem_Click);
            // 
            // deleteDrawerToolStripMenuItem
            // 
            this.deleteDrawerToolStripMenuItem.Name = "deleteDrawerToolStripMenuItem";
            resources.ApplyResources(this.deleteDrawerToolStripMenuItem, "deleteDrawerToolStripMenuItem");
            this.deleteDrawerToolStripMenuItem.Click += new System.EventHandler(this.deleteDrawerToolStripMenuItem_Click);
            // 
            // openDrawerToolStripMenuItem
            // 
            this.openDrawerToolStripMenuItem.Name = "openDrawerToolStripMenuItem";
            resources.ApplyResources(this.openDrawerToolStripMenuItem, "openDrawerToolStripMenuItem");
            this.openDrawerToolStripMenuItem.Click += new System.EventHandler(this.openDrawerToolStripMenuItem_Click);
            // 
            // assignDrawerToolStripMenuItem
            // 
            this.assignDrawerToolStripMenuItem.Name = "assignDrawerToolStripMenuItem";
            resources.ApplyResources(this.assignDrawerToolStripMenuItem, "assignDrawerToolStripMenuItem");
            this.assignDrawerToolStripMenuItem.Click += new System.EventHandler(this.assignDrawerToolStripMenuItem_Click);
            // 
            // placesToolStripMenuItem
            // 
            this.placesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createPlaceToolStripMenuItem,
            this.deletePlaceToolStripMenuItem,
            this.relinkPlaceToolStripMenuItem});
            this.placesToolStripMenuItem.Name = "placesToolStripMenuItem";
            resources.ApplyResources(this.placesToolStripMenuItem, "placesToolStripMenuItem");
            // 
            // createPlaceToolStripMenuItem
            // 
            this.createPlaceToolStripMenuItem.Name = "createPlaceToolStripMenuItem";
            resources.ApplyResources(this.createPlaceToolStripMenuItem, "createPlaceToolStripMenuItem");
            this.createPlaceToolStripMenuItem.Click += new System.EventHandler(this.createPlaceToolStripMenuItem_Click);
            // 
            // deletePlaceToolStripMenuItem
            // 
            this.deletePlaceToolStripMenuItem.Name = "deletePlaceToolStripMenuItem";
            resources.ApplyResources(this.deletePlaceToolStripMenuItem, "deletePlaceToolStripMenuItem");
            this.deletePlaceToolStripMenuItem.Click += new System.EventHandler(this.deletePlaceToolStripMenuItem_Click);
            // 
            // relinkPlaceToolStripMenuItem
            // 
            this.relinkPlaceToolStripMenuItem.Name = "relinkPlaceToolStripMenuItem";
            resources.ApplyResources(this.relinkPlaceToolStripMenuItem, "relinkPlaceToolStripMenuItem");
            this.relinkPlaceToolStripMenuItem.Click += new System.EventHandler(this.relinkPlaceToolStripMenuItem_Click);
            // 
            // mainSplitContainer
            // 
            resources.ApplyResources(this.mainSplitContainer, "mainSplitContainer");
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.drawersGroupBox);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.placesGroupBox);
            // 
            // drawersGroupBox
            // 
            this.drawersGroupBox.Controls.Add(this.drawersListBox);
            resources.ApplyResources(this.drawersGroupBox, "drawersGroupBox");
            this.drawersGroupBox.Name = "drawersGroupBox";
            this.drawersGroupBox.TabStop = false;
            // 
            // drawersListBox
            // 
            resources.ApplyResources(this.drawersListBox, "drawersListBox");
            this.drawersListBox.FormattingEnabled = true;
            this.drawersListBox.Name = "drawersListBox";
            this.drawersListBox.DoubleClick += new System.EventHandler(this.drawersListBox_DoubleClick);
            this.drawersListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.drawersListBox_KeyDown);
            // 
            // placesGroupBox
            // 
            this.placesGroupBox.Controls.Add(this.placesListBox);
            resources.ApplyResources(this.placesGroupBox, "placesGroupBox");
            this.placesGroupBox.Name = "placesGroupBox";
            this.placesGroupBox.TabStop = false;
            // 
            // placesListBox
            // 
            resources.ApplyResources(this.placesListBox, "placesListBox");
            this.placesListBox.FormattingEnabled = true;
            this.placesListBox.Name = "placesListBox";
            this.placesListBox.DoubleClick += new System.EventHandler(this.placesListBox_DoubleClick);
            this.placesListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.placesListBox_KeyDown);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.drawersGroupBox.ResumeLayout(false);
            this.placesGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip mainMenuStrip;
        private ToolStripMenuItem drawersToolStripMenuItem;
        private ToolStripMenuItem createDrawerToolStripMenuItem;
        private ToolStripMenuItem deleteDrawerToolStripMenuItem;
        private ToolStripMenuItem openDrawerToolStripMenuItem;
        private ToolStripMenuItem placesToolStripMenuItem;
        private SplitContainer mainSplitContainer;
        private GroupBox drawersGroupBox;
        private ListBox drawersListBox;
        private GroupBox placesGroupBox;
        private ListBox placesListBox;
        private ToolStripMenuItem assignDrawerToolStripMenuItem;
        private ToolStripMenuItem createPlaceToolStripMenuItem;
        private ToolStripMenuItem deletePlaceToolStripMenuItem;
        private ToolStripMenuItem relinkPlaceToolStripMenuItem;
        private ToolStripMenuItem appToolStripMenuItem;
        private ToolStripMenuItem updatesToolStripMenuItem;
        private ToolStripMenuItem manualUpdateToolStripMenuItem;
        private ToolStripMenuItem autoUpdateToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
    }
}