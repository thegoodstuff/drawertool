using TGS.DrawerTool.WinForms.Resources.Strings;

namespace TGS.DrawerTool.WinForms
{
    internal static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
#if RELEASE
            try
            {
#endif
                ApplicationConfiguration.Initialize();

                var mutex = new Mutex(true, "{35dd92bd-d81e-4ae7-a236-a4aadd5ae353}", out var mutexResult);
                if (!mutexResult)
                {
                    MessageBox.Show(null, "Another instance of the app is already running.", Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Settings.Read();

                if (args.Length == 1 && args[0] == "uninstall")
                {
                    if (Settings.Content.Places != null)
                    {
                        foreach (var place in Settings.Content.Places)
                            if (place != null)
                            {
                                Directory.Delete(place);
                                Directory.CreateDirectory(place);
                            }
                        Settings.Content.Places.Clear();
                        Settings.Save();
                    }
                }
                else
                {
                    using (mutex)
                        Application.Run(new MainForm());
                }
#if RELEASE
            }
            catch (Exception ex)
            {
                MessageBox.Show(null, $"{ex.Message}", Strings.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
#endif
        }
    }
}