﻿using TGS.DrawerTool.WinForms.Resources.AppResources;

namespace TGS.DrawerTool.WinForms
{
    public partial class SetupForm : Form
    {
        public string DrawersBasePath { get => drawersBaseTextBox.Text; set => drawersBaseTextBox.Text = value; }

        public bool AllowUpdates { get => allowUpdatesCheckBox.Checked; set => allowUpdatesCheckBox.Checked = value; }

        public SetupForm()
        {
            InitializeComponent();
            DrawersBasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), AppResources.AppName, AppResources.DefaultDrawerBaseSubdir);
        }

        private void browseDrawersBaseButton_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.SelectedPath = DrawersBasePath;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                DrawersBasePath = dialog.SelectedPath;
            }
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
