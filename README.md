# DrawerTool

A tool to place different folders at the same path at different times.

A sort of symlink manager, with a specific objective.

## Installation

Download the [latest
release](https://gitlab.com/thegoodstuff/drawertool/-/releases) from GitLab.

The file you're looking for is named `DrawerTool-x.x.x-Setup.exe`.

This application only runs on [64-bit Windows versions that are supported by
.NET
6](https://github.com/dotnet/core/blob/main/release-notes/6.0/supported-os.md#windows).

## Usage

### First run

On first run, you'll be asked to select a folder to use as the storage location
for your drawers. This is the true location of the drawers (symlinks created
with this app will point to its subfolders).  
The application must be reinstalled to change the storage location.

You may also want to disable automatic update checking to stop this application
from connecting to the Internet.

### General usage

Drawers can be linked to "places".  
Once you have created a drawer, you can link it to a place. (All these actions
can be performed from the menu bar).

Once linked, the "place" will appear to contain the contents of a drawer.

When unlinking a place it will become a regular empty folder.  
Deleting a drawer will also unlink all places it is linked to.

Linking or relinking a drawer requires admin permissions. (Windows limitation)

### Shortcuts

- Double-click a drawer, or select a drawer and then press Enter, to view its
  contents.
- Double-click a place, or select a place and then press Enter to change the
  drawer it is linked to.
- Select a drawer and then press Delete or Backspace to delete it.
- Select a place and then press Delete or Backspace to unlink it.

## Updates

The update checker looks at the releases of this GitLab project. It only
connects to GitLab.com.

When an update is available, the application will display a prompt to visit the
release page to download the new version.

## Uninstallation

When uninstalling, all your places will be unlinked and turned into empty
folders.

Your drawers will not be deleted.
